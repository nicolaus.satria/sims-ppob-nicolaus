class ProfileResponse {
  String email;
  String firstName;
  String lastName;
  String profileImage;

  ProfileResponse({
    required this.email,
    required this.firstName,
    required this.lastName,
    required this.profileImage,
  });

  factory ProfileResponse.fromJson(Map<String, dynamic> json) {
    return ProfileResponse(
      email: json['email'] ?? '',
      firstName: json['first_name'] ?? '',
      lastName: json['last_name'] ?? '',
      profileImage: json['profile_image'] ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'email': email,
      'first_name': firstName,
      'last_name': lastName,
      'profile_image': profileImage,
    };
  }
}
