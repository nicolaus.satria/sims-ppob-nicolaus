import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sims_ppob/model/api.dart';
import 'package:sims_ppob/model/transaction.dart';
import 'package:sims_ppob/model/transaction_history.dart';

class TransactionProvider extends ChangeNotifier {
  List<TransactionData> transactions = [];
  List<TransactionRecord> transactionRecords = [];

  Future<void> fetchTransactions(String? token) async {
    final url = Uri.parse('$baseUrl/transaction');

    final response = await http.get(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      final parsedData = json.decode(response.body);
      final transactionList = parsedData['data']['records'] as List;

      transactions = transactionList
          .map((data) => TransactionData.fromJson(data))
          .toList();

      notifyListeners();
    } else {
      throw Exception('Failed to load transactions');
    }
  }
}

class TransactionHistoryProvider extends ChangeNotifier {
  List<TransactionRecord> transactionRecords = [];

  Future<void> loadTransactionHistory(
      String? token, int offset, int limit) async {
    try {
      final url =
          Uri.parse('$baseUrl/transaction/history?offset=$offset&limit=$limit');

      final response = await http.get(
        url,
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200) {
        final parsedData = json.decode(response.body);
        final List<dynamic> records = parsedData['data']['records'];

        if (offset == 0) {
          transactionRecords = records
              .map((record) => TransactionRecord.fromJson(record))
              .toList();
        } else {
          transactionRecords.addAll(
            records.map((record) => TransactionRecord.fromJson(record)),
          );
        }
        notifyListeners();
      } else {
        throw Exception('Failed to load transaction history');
      }
    } catch (error) {
      throw Exception('Error: $error');
    }
  }
}
