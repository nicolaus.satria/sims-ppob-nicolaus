import 'package:flutter/material.dart';
import 'package:sims_ppob/ui/dashboard/home/widget/saldo.dart';
import 'package:sims_ppob/ui/transaction/widget/transaction_history.dart';

class Transaksi extends StatelessWidget {
  const Transaksi({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Transaction',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: const Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Saldo(),
            SizedBox(height: 50),
            Text(
              'Transaksi',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),
            ),
            Expanded(child: TransactionHistoryPage()),
          ],
        ),
      ),
    );
  }
}
