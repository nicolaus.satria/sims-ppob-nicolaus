import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/ui/login/provider/login_provider.dart';
import 'package:sims_ppob/ui/transaction/provider/transaction_provider.dart';

class TransactionHistoryPage extends StatefulWidget {
  const TransactionHistoryPage({super.key});

  @override
  _TransactionHistoryPageState createState() => _TransactionHistoryPageState();
}

class _TransactionHistoryPageState extends State<TransactionHistoryPage> {
  bool _isLoading = true;
  bool _isLoadingMore = false;
  int _offset = 0;
  final int _limit = 5;

  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _initializeLocale();
    _loadTransactionHistory();
  }

  Future<void> _initializeLocale() async {
    await initializeDateFormatting('id_ID', null);
  }

  Future<void> _loadTransactionHistory({bool isLoadMore = false}) async {
    setState(() {
      if (isLoadMore) {
        _isLoadingMore = true;
      } else {
        _isLoading = true;
      }
    });

    try {
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      final token = authProvider.token;

      final transactionProvider =
          Provider.of<TransactionHistoryProvider>(context, listen: false);
      await transactionProvider.loadTransactionHistory(token, _offset, _limit);

      setState(() {
        _isLoading = false;
        _isLoadingMore = false;
      });

      if (isLoadMore) {
        _scrollToBottom(); // Scroll to the bottom when "Show More" is pressed
      }
    } catch (e) {
      setState(() {
        _isLoading = false;
        _isLoadingMore = false;
      });
    }
  }

  void _scrollToBottom() {
    Future.delayed(const Duration(milliseconds: 300), () {
      if (_scrollController.hasClients) {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeInOut,
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TransactionHistoryProvider>(
      builder: (context, provider, _) {
        final transactionRecords = provider.transactionRecords;

        if (_isLoading && transactionRecords.isEmpty) {
          return const Center(child: CircularProgressIndicator());
        }

        if (transactionRecords.isEmpty) {
          return const Center(
            child: Text('No transaction history available.'),
          );
        }

        return Column(
          children: [
            Expanded(
              child: ListView.builder(
                controller: _scrollController,
                itemCount: transactionRecords.length,
                itemBuilder: (context, index) {
                  final record = transactionRecords[index];
                  return Container(
                    margin: const EdgeInsets.only(top: 16),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 1),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              record.transactionType == "TOPUP"
                                  ? "+ Rp.${record.totalAmount}"
                                  : "- Rp.${record.totalAmount}",
                              style: TextStyle(
                                color: record.transactionType == "TOPUP"
                                    ? Colors.green
                                    : Colors.red,
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                              ),
                            ),
                            Text(
                              _formatDate(record.createdOn),
                              style: const TextStyle(color: Colors.grey),
                            ),
                          ],
                        ),
                        Text(record.description),
                      ],
                    ),
                  );
                },
              ),
            ),
            if (_isLoadingMore)
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: CircularProgressIndicator(),
              ),
            TextButton(
              onPressed: _isLoadingMore
                  ? null
                  : () {
                      setState(() {
                        _offset += _limit;
                      });
                      _loadTransactionHistory(isLoadMore: true);
                    },
              child: const Text(
                'Show more',
                style: TextStyle(
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  String _formatDate(DateTime dateTime) {
    return DateFormat('d MMMM yyyy HH:mm', 'id_ID').format(dateTime) + ' WIB';
  }
}
