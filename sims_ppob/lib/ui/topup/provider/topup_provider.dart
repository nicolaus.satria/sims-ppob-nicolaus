import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sims_ppob/model/api.dart';
import 'package:sims_ppob/model/top_up.dart';

class TopUpProvider with ChangeNotifier {
  final TextEditingController _textEditingController = TextEditingController();
  String _selectedNominal = '';
  bool _isButtonEnabled = false;
  String? _errorMessage;

  TextEditingController get textEditingController => _textEditingController;
  String get selectedNominal => _selectedNominal;
  bool get isButtonEnabled => _isButtonEnabled;
  String? get errorMessage => _errorMessage;

  TopUpProvider() {
    _textEditingController.addListener(() {
      onTextFieldChanged(_textEditingController.text);
    });
  }
  void onTextFieldChanged(String text) {
    final int? amount = int.tryParse(text);
    if (amount == null || amount < 10000) {
      _isButtonEnabled = false;
      _errorMessage = 'Minimal top-up Rp10.000';
    } else if (amount > 1000000) {
      _isButtonEnabled = false;
      _errorMessage = 'Maksimal top-up Rp1.000.000';
    } else {
      _isButtonEnabled = true;
      _errorMessage = null;
    }
    notifyListeners();
  }

  void setSelectedNominal(String nominal) {
    _selectedNominal = nominal;
    _textEditingController.text = nominal;
    onTextFieldChanged(nominal);
  }

  Future<void> performTopUp(BuildContext context, String? token) async {
    final String nominal = _textEditingController.text;
    final url = '$baseUrl/topup';

    final response = await http.post(
      Uri.parse(url),
      body: json.encode({"top_up_amount": nominal}),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );
    final responseData = json.decode(response.body);
    final topUpResponse = TopUpResponse.fromJson(responseData);
    if (response.statusCode == 200) {
      _showTopUpDialog(
          context, topUpResponse.status == 0, nominal, topUpResponse.message);
    } else {
      _showErrorSnackBar(context, 'Terjadi kesalahan saat melakukan top up.');
    }
  }

  void _showTopUpDialog(
      BuildContext context, bool isSuccess, String nominal, String message) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          contentPadding: const EdgeInsets.all(20.0),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                isSuccess ? Icons.check_circle : Icons.cancel_outlined,
                color: isSuccess ? Colors.green : Colors.red,
                size: 80,
              ),
              const SizedBox(height: 16),
              Text(
                message,
                style: const TextStyle(
                  fontSize: 16,
                  color: Colors.black87,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 8),
              Text(
                'Rp$nominal',
                style: const TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              const SizedBox(height: 16),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pop(); // Close dialog
                  Navigator.pushReplacementNamed(
                      context, '/dashboard'); // Go to Home
                },
                child: const Text(
                  'Kembali ke Beranda',
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.red,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  void _showErrorSnackBar(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
      ),
    );
  }
}
