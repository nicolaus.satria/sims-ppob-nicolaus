import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/ui/topup/provider/topup_provider.dart';

class NominalTransaksi extends StatelessWidget {
  const NominalTransaksi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final topUpProvider = Provider.of<TopUpProvider>(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextField(
          controller: topUpProvider.textEditingController,
          onChanged: topUpProvider.onTextFieldChanged,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            contentPadding:
                const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
            hintText: 'Masukkan nominal Top Up',
            hintStyle: const TextStyle(color: Colors.grey, fontSize: 14),
            prefixIcon: const Icon(Icons.credit_card),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: const BorderSide(color: Colors.blue, width: 2.0),
            ),
          ),
        ),
        const SizedBox(height: 8),
        if (topUpProvider.errorMessage != null)
          Text(
            topUpProvider.errorMessage!,
            style: const TextStyle(color: Colors.red, fontSize: 12),
          ),
      ],
    );
  }
}
