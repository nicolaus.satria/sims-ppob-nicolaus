import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/ui/topup/provider/topup_provider.dart';
import 'package:sims_ppob/ui/login/provider/login_provider.dart';

class TombolTopUp extends StatelessWidget {
  const TombolTopUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthProvider>(builder: (context, authProvider, _) {
      final token = authProvider.token;
      final topUpProvider = Provider.of<TopUpProvider>(context);

      return Align(
        alignment: Alignment.center,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: ElevatedButton(
            onPressed: topUpProvider.isButtonEnabled
                ? () =>
                    _showTopUpConfirmationDialog(context, topUpProvider, token)
                : null,
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.red,
              foregroundColor: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            child: const Text(
              'Top Up',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ),
      );
    });
  }

  void _showTopUpConfirmationDialog(
      BuildContext context, TopUpProvider topUpProvider, String? token) {
    final nominal = topUpProvider.selectedNominal;

    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.0),
          ),
          content: Container(
            constraints: BoxConstraints(
              maxWidth: MediaQuery.of(context).size.width,
              maxHeight: MediaQuery.of(context).size.height * 0.5,
            ),
            margin: const EdgeInsets.all(16.0),
            // padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  'assets/images/Logo.png',
                  scale: 2.5,
                ),
                const SizedBox(height: 16),
                const Text(
                  'Anda yakin untuk Topup sebesar?',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black87,
                  ),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 8),
                Text(
                  'Rp$nominal ?',
                  style: const TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                const SizedBox(height: 24),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                    topUpProvider.performTopUp(context, token);
                  },
                  child: const Text(
                    'Ya, lanjutkan Top Up',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const SizedBox(height: 12),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: const Text(
                    'Batalkan',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
