import 'package:flutter/material.dart';
import 'package:sims_ppob/ext/my_list.dart';
import 'package:sims_ppob/ui/topup/provider/topup_provider.dart';

class GridNominal extends StatelessWidget {
  final TopUpProvider topUpProvider;

  const GridNominal({
    Key? key,
    required this.topUpProvider,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 150,
        childAspectRatio: 3,
        crossAxisSpacing: 10,
        mainAxisSpacing: 25,
      ),
      itemCount: MyList.nominal.length,
      itemBuilder: (context, index) {
        final selected = MyList.nominal[index] == topUpProvider.selectedNominal;

        return InkWell(
          onTap: () {
            final selectedNominal = MyList.nominal[index];
            topUpProvider.setSelectedNominal(selectedNominal);
          },
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey,
                width: selected ? 2 : 1,
              ),
              borderRadius: BorderRadius.circular(10),
              color: selected ? Colors.grey.withOpacity(0.1) : Colors.white,
            ),
            child: Center(
              child: Text(
                'Rp${MyList.nominal[index]}',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: selected ? FontWeight.bold : FontWeight.normal,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
