import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/ui/dashboard/home/widget/saldo.dart';
import 'package:sims_ppob/ui/topup/provider/topup_provider.dart';
import 'package:sims_ppob/ui/topup/widget/grid_nominal.dart';
import 'package:sims_ppob/ui/topup/widget/topup_button.dart';
import 'package:sims_ppob/ui/topup/widget/topup_saldo.dart';
import 'package:sims_ppob/ui/topup/widget/transaction_nominal.dart';

class TopUp extends StatelessWidget {
  const TopUp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => TopUpProvider(),
      child: _TopUpContent(),
    );
  }
}

class _TopUpContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final topUpProvider = Provider.of<TopUpProvider>(context);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Top Up',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Saldo(),
            const SizedBox(
              height: 40,
            ),
            const Text(
              'Silahkan masukkan',
              style: TextStyle(
                fontSize: 22,
              ),
            ),
            const Text(
              'nominal Top Up',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            const NominalTransaksi(),
            const SizedBox(
              height: 20,
            ),
            GridNominal(topUpProvider: topUpProvider),
            const SizedBox(
              height: 20,
            ),
            TombolTopUp(),
          ],
        ),
      ),
    );
  }
}
