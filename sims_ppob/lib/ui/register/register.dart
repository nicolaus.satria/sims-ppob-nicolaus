import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/ui/login/provider/login_provider.dart';

class RegisterView extends StatefulWidget {
  const RegisterView({super.key});

  @override
  State<RegisterView> createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  final ScrollController _scrollController = ScrollController();
  final FocusNode _confirmPasswordFocusNode = FocusNode();

  bool isConfirmPasswordVisible = false;
  bool isPasswordVisible = false;

  String email = '';
  String firstName = '';
  String lastName = '';
  String password = '';
  String confirmPassword = '';
  bool isPasswordMismatch = false;

  @override
  void initState() {
    super.initState();
    _confirmPasswordFocusNode.addListener(() {
      if (_confirmPasswordFocusNode.hasFocus) {
        _scrollToField();
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _confirmPasswordFocusNode.dispose();
    super.dispose();
  }

  void _scrollToField() {
    Future.delayed(const Duration(milliseconds: 300), () {
      _scrollController.animateTo(
        _scrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 500),
        curve: Curves.easeInOut,
      );
    });
  }

  void _validatePasswords() {
    setState(() {
      isPasswordMismatch = password != confirmPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Register'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: SingleChildScrollView(
            controller: _scrollController,
            physics: const BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildHeader(),
                const SizedBox(height: 16),
                _buildTitle(),
                const SizedBox(height: 32),
                _buildEmailField(authProvider),
                const SizedBox(height: 32),
                _buildFirstNameField(authProvider),
                const SizedBox(height: 32),
                _buildLastNameField(authProvider),
                const SizedBox(height: 32),
                _buildPasswordField(authProvider),
                const SizedBox(height: 32),
                _buildConfirmPasswordField(),
                if (isPasswordMismatch) _buildPasswordError(),
                const SizedBox(height: 32),
                _buildRegisterButton(context, authProvider),
                const SizedBox(height: 16),
                _buildLoginOption(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildHeader() {
    return Center(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset('assets/images/Logo.png', height: 30),
          const SizedBox(width: 8),
          const Text(
            'SIMS PPOB',
            style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }

  Widget _buildTitle() {
    return const Text(
      'Lengkapi data untuk membuat akun',
      style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
      textAlign: TextAlign.center,
    );
  }

  Widget _buildTextField({
    required ValueChanged<String> onChanged,
    required String hintText,
    required IconData prefixIcon,
    bool obscureText = false,
    Widget? suffixIcon,
    FocusNode? focusNode,
  }) {
    return TextFormField(
      onChanged: onChanged,
      obscureText: obscureText,
      focusNode: focusNode,
      decoration: InputDecoration(
        hintText: hintText,
        prefixIcon: Icon(prefixIcon, color: Colors.grey),
        suffixIcon: suffixIcon,
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.grey, width: 1.5),
          borderRadius: BorderRadius.circular(10),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.orange, width: 2.0),
          borderRadius: BorderRadius.circular(10),
        ),
      ),
    );
  }

  Widget _buildEmailField(AuthProvider authProvider) {
    return _buildTextField(
      onChanged: (value) => setState(() => email = value),
      hintText: 'Email',
      prefixIcon: Icons.email,
    );
  }

  Widget _buildFirstNameField(AuthProvider authProvider) {
    return _buildTextField(
      onChanged: (value) => setState(() => firstName = value),
      hintText: 'Nama Depan',
      prefixIcon: Icons.person,
    );
  }

  Widget _buildLastNameField(AuthProvider authProvider) {
    return _buildTextField(
      onChanged: (value) => setState(() => lastName = value),
      hintText: 'Nama Belakang',
      prefixIcon: Icons.person,
    );
  }

  Widget _buildPasswordField(AuthProvider authProvider) {
    return _buildTextField(
      onChanged: (value) {
        setState(() {
          password = value;
        });
        _validatePasswords();
      },
      hintText: 'Password',
      prefixIcon: Icons.lock,
      obscureText: !isPasswordVisible,
      suffixIcon: IconButton(
        onPressed: () => setState(() {
          isPasswordVisible = !isPasswordVisible;
        }),
        icon: Icon(
          isPasswordVisible ? Icons.visibility : Icons.visibility_off,
        ),
      ),
    );
  }

  Widget _buildConfirmPasswordField() {
    return _buildTextField(
      onChanged: (value) {
        setState(() {
          confirmPassword = value;
        });
        _validatePasswords();
      },
      hintText: 'Konfirmasi Password',
      prefixIcon: Icons.lock,
      obscureText: !isConfirmPasswordVisible,
      suffixIcon: IconButton(
        onPressed: () => setState(() {
          isConfirmPasswordVisible = !isConfirmPasswordVisible;
        }),
        icon: Icon(
          isConfirmPasswordVisible ? Icons.visibility : Icons.visibility_off,
        ),
      ),
      focusNode: _confirmPasswordFocusNode,
    );
  }

  Widget _buildPasswordError() {
    return const Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: EdgeInsets.only(top: 4.0),
        child: Text(
          'Password tidak sama',
          style: TextStyle(color: Colors.red, fontSize: 12),
        ),
      ),
    );
  }

  Widget _buildRegisterButton(BuildContext context, AuthProvider authProvider) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () async {
          if (isPasswordMismatch) return;

          final response = await authProvider.register(
            email: email,
            firstName: firstName,
            lastName: lastName,
            password: password,
          );

          if (response.status == 0) {
            _showSuccessDialog(context, response.message);
          } else {
            _showErrorDialog(context, response.message);
          }
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.red,
          minimumSize: const Size.fromHeight(50),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        ),
        child: const Text('Registrasi', style: TextStyle(color: Colors.white)),
      ),
    );
  }

  void _showSuccessDialog(BuildContext context, String? message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Registrasi Berhasil'),
        content: Text(message ?? ''),
        actions: [
          TextButton(
            onPressed: () => Navigator.pushReplacementNamed(context, '/login'),
            child: const Text('OK'),
          ),
        ],
      ),
    );
  }

  void _showErrorDialog(BuildContext context, String? message) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Registrasi Gagal'),
        content: Text(message ?? ''),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('Tutup'),
          ),
        ],
      ),
    );
  }

  Widget _buildLoginOption(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text('Sudah punya akun? Login '),
        TextButton(
          onPressed: () => Navigator.pushReplacementNamed(context, '/login'),
          child: const Text('di sini'),
        ),
      ],
    );
  }
}
