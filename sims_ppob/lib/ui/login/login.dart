import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/ext/string_ext.dart';
import 'package:sims_ppob/ui/login/provider/login_provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final authProvider = Provider.of<AuthProvider>(context);

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            const Spacer(),
            _buildHeader(),
            const SizedBox(height: 32),
            const Text(
              'Masuk atau buat akun untuk memulai',
              style: TextStyle(
                fontSize: 32,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 32),
            _buildEmailField(authProvider),
            const SizedBox(height: 24),
            _buildPasswordField(authProvider),
            const Spacer(),
            _buildLoginButton(context, authProvider),
            _buildRegisterOption(context),
            const Spacer(),
          ],
        ),
      ),
    );
  }

  Widget _buildHeader() {
    return Container(
      constraints: const BoxConstraints(maxWidth: 180),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Image.asset(
            'assets/images/Logo.png',
            height: 30,
          ),
          const Text(
            'SIMS PPOB',
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildEmailField(AuthProvider authProvider) {
    return TextFormField(
      onChanged: (value) => authProvider.setEmail(value),
      decoration: InputDecoration(
        hintText: 'Email',
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.grey, width: 1.5),
          borderRadius: BorderRadius.circular(8),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.orange, width: 2.0),
          borderRadius: BorderRadius.circular(8),
        ),
        contentPadding:
            const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
        hintStyle: const TextStyle(fontSize: 14),
        prefixIcon: const Icon(Icons.alternate_email, color: Colors.grey),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Email tidak boleh kosong';
        } else if (!value.isValidEmail) {
          return 'Email tidak valid';
        }
        return null;
      },
    );
  }

  Widget _buildPasswordField(AuthProvider authProvider) {
    return TextFormField(
      onChanged: (value) => authProvider.setPassword(value),
      obscureText: !authProvider.isPasswordVisible,
      decoration: InputDecoration(
        hintText: 'Password',
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.grey, width: 1.5),
          borderRadius: BorderRadius.circular(8),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Colors.orange, width: 2.0),
          borderRadius: BorderRadius.circular(8),
        ),
        contentPadding:
            const EdgeInsets.symmetric(vertical: 10, horizontal: 12),
        hintStyle: const TextStyle(fontSize: 14),
        prefixIcon: const Icon(Icons.lock, color: Colors.grey),
        suffixIcon: IconButton(
          onPressed: authProvider.togglePasswordVisibility,
          icon: Icon(
            authProvider.isPasswordVisible
                ? Icons.visibility
                : Icons.visibility_off,
            color: Colors.grey,
          ),
        ),
      ),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Password tidak boleh kosong';
        } else if (value.length < 8) {
          return 'Password harus memiliki setidaknya 8 karakter';
        }
        return null;
      },
    );
  }

  Widget _buildLoginButton(BuildContext context, AuthProvider authProvider) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        onPressed: () async {
          final response = await authProvider.logIn(
            email: authProvider.email,
            password: authProvider.password,
          );

          if (response != null && response.status == 0) {
            Navigator.pushReplacementNamed(context, '/dashboard');
          } else {
            // );
            showFloatingSnackBar(
                context, response?.message ?? 'Terjadi kesalahan saat login.');
          }
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.red,
          fixedSize: const Size(200, 50),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ),
        child: const Text(
          'Masuk',
          style: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget _buildRegisterOption(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          'Belum punya akun? register',
          style: TextStyle(fontSize: 14),
        ),
        TextButton(
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/register');
          },
          child: const Text(
            'di sini',
            style: TextStyle(fontSize: 12),
          ),
        ),
      ],
    );
  }

  void showFloatingSnackBar(BuildContext context, String message) {
    final overlay = Overlay.of(context);
    final overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        bottom: MediaQuery.of(context).size.height * 0.03,
        left: 16,
        right: 16,
        child: Material(
          color: Colors.transparent,
          child: Container(
            padding: const EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 252, 156, 12)
                  .withOpacity(0.2), // Custom opacity
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    message,
                    style: const TextStyle(
                      color: Colors.orange,
                      fontSize: 12,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    overlay.insert(overlayEntry);
    Future.delayed(const Duration(seconds: 3), () {
      overlayEntry.remove();
    });
  }
}
