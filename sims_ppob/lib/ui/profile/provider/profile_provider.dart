import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sims_ppob/model/api.dart';
import 'package:sims_ppob/model/profile.dart';

class ProfileProvider with ChangeNotifier {
  ProfileResponse? _profile;
  bool _isLoading = false;
  String? _errorMessage;

  ProfileResponse? get profile => _profile;
  bool get isLoading => _isLoading;
  String? get errorMessage => _errorMessage;

  Future<void> fetchProfile(String token) async {
    final url = Uri.parse('$baseUrl/profile');
    _setLoading(true);

    try {
      final response = await http.get(
        url,
        headers: {'Authorization': 'Bearer $token'},
      );
      if (response.statusCode == 200) {
        final jsonResponse = json.decode(response.body);
        _profile = ProfileResponse.fromJson(jsonResponse['data']);
      } else {
        _setErrorMessage('Failed to fetch profile');
      }
    } catch (error) {
      _setErrorMessage('Error fetching profile: $error');
    } finally {
      _setLoading(false);
    }
  }

  Future<void> updateProfileImage(String token, File imageFile) async {
    final url = Uri.parse('$baseUrl/profile/image');
    _setLoading(true);

    try {
      final request = http.MultipartRequest('PUT', url);
      request.headers['Authorization'] = 'Bearer $token';

      final stream = http.ByteStream(imageFile.openRead());
      final length = await imageFile.length();
      final multipartFile = http.MultipartFile('file', stream, length,
          filename: 'profile_image.jpg');

      request.files.add(multipartFile);

      final response = await request.send();
      final responseString = await response.stream.bytesToString();

      if (response.statusCode == 200) {
        final jsonResponse = json.decode(responseString);
        _profile = ProfileResponse.fromJson(jsonResponse['data']);
        notifyListeners();
      } else {
        _setErrorMessage('Failed to update profile image');
      }
    } catch (error) {
      _setErrorMessage('Error updating profile image: $error');
    } finally {
      _setLoading(false);
    }
  }

  Future<void> updateProfile(
      String token, String firstName, String lastName) async {
    final url = Uri.parse('$baseUrl/profile/update');
    _setLoading(true);

    try {
      final body = json.encode({
        'first_name': firstName,
        'last_name': lastName,
      });

      final headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      };

      final response = await http.put(url, headers: headers, body: body);
      debugPrint("prof " + token);

      if (response.statusCode == 200) {
        final jsonResponse = json.decode(response.body);
        _profile = ProfileResponse.fromJson(jsonResponse['data']);
        notifyListeners();
      } else if (response.statusCode == 401) {
        _setErrorMessage('Token expired. Please log in again.');
      } else {
        _setErrorMessage('Failed to update profile');
      }
    } catch (error) {
      _setErrorMessage('Error updating profile: $error');
    } finally {
      _setLoading(false);
    }
  }

  void _setLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  void _setErrorMessage(String? message) {
    _errorMessage = message;
    notifyListeners();
  }
}
