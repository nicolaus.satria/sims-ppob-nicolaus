import 'package:flutter/material.dart';
import 'package:sims_ppob/ui/dashboard/home/widget/saldo.dart';
import 'package:sims_ppob/ui/dashboard/payment/provider/payment_provider.dart';

class Payment extends StatelessWidget {
  final String serviceCode;
  final String serviceName;
  final String serviceIcon;
  final int totalAmount;

  const Payment({
    super.key,
    required this.serviceCode,
    required this.serviceName,
    required this.serviceIcon,
    required this.totalAmount,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Pembayaran',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        physics: const ScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 24.0),
                child: Saldo(),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: Text(
                  'Pembayaran',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.start,
                ),
              ),
              Row(
                children: [
                  Image.network(
                    serviceIcon,
                    width: 50,
                    height: 50,
                    fit: BoxFit.contain,
                  ),
                  Text(
                    serviceName,
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: TextField(
                  controller:
                      TextEditingController(text: totalAmount.toString()),
                  readOnly: true,
                  decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(
                      vertical: 16,
                    ),
                    prefixIcon: const Icon(Icons.credit_card),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                ),
              ),
              Center(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.red,
                      foregroundColor: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                    onPressed: () {
                      performTransaction(context, serviceCode, totalAmount);
                    },
                    child: const Text('Bayar'),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
