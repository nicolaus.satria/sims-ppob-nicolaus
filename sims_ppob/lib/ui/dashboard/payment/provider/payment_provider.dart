import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:sims_ppob/model/api.dart';
import 'package:sims_ppob/model/transaction.dart';
import 'package:sims_ppob/ui/login/provider/login_provider.dart';

Future<void> performTransaction(
    BuildContext context, String serviceCode, int totalAmount) async {
  final authProvider = Provider.of<AuthProvider>(context, listen: false);
  final token = authProvider.token;
  final url = '$baseUrl/transaction';

  final requestBody = {
    "service_code": serviceCode,
  };

  try {
    final response = await http.post(
      Uri.parse(url),
      body: json.encode(requestBody),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    final responseData = json.decode(response.body);
    final transactionResponse = TransactionResponse.fromJson(responseData);

    if (response.statusCode == 200) {
      showTopUpDialog(context, transactionResponse.status == 0,
          totalAmount.toString(), transactionResponse.message);
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text('Terjadi kesalahan saat melakukan transaksi.'),
        ),
      );
    }
  } catch (e) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          contentPadding: const EdgeInsets.all(20.0),
          content: const Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Icon(
                Icons.cancel_outlined,
                color: Colors.red,
                size: 80,
              ),
              SizedBox(height: 16),
              Text(
                'Terjadi kesalahan',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black87,
                ),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        );
      },
    );
  }
}

void showTopUpDialog(
    BuildContext context, bool isSuccess, String nominal, String message) {
  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        contentPadding: const EdgeInsets.all(20.0),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              isSuccess ? Icons.check_circle : Icons.cancel_outlined,
              color: isSuccess ? Colors.green : Colors.red,
              size: 80,
            ),
            const SizedBox(height: 16),
            Text(
              message,
              style: const TextStyle(
                fontSize: 16,
                color: Colors.black87,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 8),
            Text(
              'Rp$nominal',
              style: const TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            const SizedBox(height: 16),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pop(); // Close dialog
                Navigator.pushReplacementNamed(
                    context, '/dashboard'); // Go to Home
              },
              child: const Text(
                'Kembali ke Beranda',
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.red,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      );
    },
  );
}
