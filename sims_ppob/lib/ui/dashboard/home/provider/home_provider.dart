import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sims_ppob/model/api.dart';
import 'package:sims_ppob/model/balance.dart';
import 'package:sims_ppob/model/banner.dart';
import 'package:sims_ppob/model/profile.dart';
import 'package:sims_ppob/model/services.dart';

class HomeProvider with ChangeNotifier {
  int _balance = 0;
  int get balance => _balance;
  bool _isBalanceVisible = false;
  bool get isBalanceVisible => _isBalanceVisible;
  List<Service> _services = [];
  List<Service> get services => _services;
  List<ProfileResponse> _profiles = [];
  List<ProfileResponse> get profiles => _profiles;

  void toggleBalanceVisibility() {
    _isBalanceVisible = !_isBalanceVisible;
    notifyListeners();
  }

  Future<void> fetchProfile(String? token) async {
    try {
      final response = await http.get(
        Uri.parse('$baseUrl/profile'),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200) {
        final Map<String, dynamic> data = json.decode(response.body);

        if (data['status'] == 0) {
          final profile = ProfileResponse.fromJson(data['data']);
          _profiles = [profile]; // Store as a list with one profile
          notifyListeners();
        } else {
          throw Exception('Failed to load profile.');
        }
      } else {
        throw Exception('Failed to load profile.');
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<void> fetchBalance(String? token) async {
    final apiUrl = '$baseUrl/balance';

    try {
      final response = await http.get(
        Uri.parse(apiUrl),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      debugPrint("banner2" + response.body);
      if (response.statusCode == 200) {
        final Map<String, dynamic> responseData = json.decode(response.body);
        final balanceModel = BalanceModel.fromJson(responseData);
        _balance = balanceModel.data.balance;
        notifyListeners();
      } else {
        throw Exception('Failed to load balance');
      }
    } catch (e) {
      throw Exception('Failed to load balance');
    }
  }

  Future<BannerResponse> fetchBanners(String? token) async {
    final String apiUrl = '$baseUrl/banner';
    final response = await http.get(
      Uri.parse(apiUrl),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = json.decode(response.body);
      return BannerResponse.fromJson(responseData);
    } else {
      throw Exception('Failed to load banners');
    }
  }

  Future<void> fetchServices(String? token) async {
    try {
      final response = await http.get(
        Uri.parse('$baseUrl/services'),
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      debugPrint("banner1" + token!);
      if (response.statusCode == 200) {
        final Map<String, dynamic> data = json.decode(response.body);
        if (data['status'] == 0) {
          final List<dynamic> serviceData = data['data'];
          final List<Service> serviceList = serviceData
              .map((serviceData) => Service.fromJson(serviceData))
              .toList();

          _services = serviceList;
          notifyListeners();
        } else {
          throw Exception('Failed to load services');
        }
      } else {
        throw Exception('Failed to load services');
      }
    } catch (e) {
      rethrow;
    }
  }
}
