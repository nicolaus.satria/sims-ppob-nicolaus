import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/model/services.dart';
import 'package:sims_ppob/ui/dashboard/home/provider/home_provider.dart';
import 'package:sims_ppob/ui/login/provider/login_provider.dart';
import 'package:sims_ppob/ui/transaction/payment.dart';

class ServiceMenuGrid extends StatefulWidget {
  const ServiceMenuGrid({super.key});

  @override
  _ServiceMenuGridState createState() => _ServiceMenuGridState();
}

class _ServiceMenuGridState extends State<ServiceMenuGrid> {
  bool _isLoading = true;
  String? _errorMessage;

  @override
  void initState() {
    super.initState();
    _fetchServices(); // Fetch services only once on initialization
  }

  Future<void> _fetchServices() async {
    try {
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      final homeProvider = Provider.of<HomeProvider>(context, listen: false);
      final token = authProvider.token;

      await homeProvider.fetchServices(token);
      setState(() {
        _isLoading = false;
      });
    } catch (error) {
      setState(() {
        _isLoading = false;
        _errorMessage = error.toString();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context);
    final services = homeProvider.services;

    if (_isLoading) {
      return const Center(child: CircularProgressIndicator());
    }

    if (_errorMessage != null) {
      return Center(child: Text('Error: $_errorMessage'));
    }

    return GridView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 6,
        crossAxisSpacing: 1,
        mainAxisSpacing: 20.0,
      ),
      itemCount: services.length,
      itemBuilder: (context, index) {
        final service = services[index];
        return ServiceCard(service: service);
      },
    );
  }
}

class ServiceCard extends StatelessWidget {
  final Service service;

  const ServiceCard({super.key, required this.service});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => Payment(
              serviceCode: service.serviceCode,
              serviceName: service.serviceName,
              serviceIcon: service.serviceIcon,
              totalAmount: service.serviceTariff,
            ),
          ),
        );
      },
      child: Column(
        children: <Widget>[
          Image.network(
            service.serviceIcon,
            width: 50,
            height: 50,
            fit: BoxFit.contain,
          ),
          Expanded(
            child: Center(
              child: Text(
                softWrap: true,
                service.serviceName,
                style: const TextStyle(
                  fontSize: 12,
                ),
                maxLines: 2,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
