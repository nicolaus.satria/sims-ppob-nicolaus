import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/ui/dashboard/home/provider/home_provider.dart';
import 'package:sims_ppob/ui/login/provider/login_provider.dart';

class ProfileName extends StatelessWidget {
  const ProfileName({super.key});

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final token = authProvider.token;

    return FutureBuilder<void>(
      future: homeProvider.fetchProfile(token),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return Center(child: Text('Error: ${snapshot.error}'));
        } else {
          final profiles = homeProvider.profiles;

          if (profiles.isEmpty) {
            return const Center(
              child: Text('No profile data available.'),
            );
          }

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Selamat datang,',
                style: TextStyle(fontSize: 20),
              ),
              ...profiles.map((profile) {
                return Text(
                  '${profile.firstName} ${profile.lastName} ',
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                );
              }).toList(),
            ],
          );
        }
      },
    );
  }
}
