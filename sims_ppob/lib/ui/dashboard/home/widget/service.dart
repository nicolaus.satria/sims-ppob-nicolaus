// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'package:sims_ppob/model/services.dart';
// import 'package:sims_ppob/ui/dashboard/home/provider/home_provider.dart';
// import 'package:sims_ppob/ui/login/provider/login_provider.dart';
// import 'package:sims_ppob/ui/transaction/payment.dart';

// class ServiceGrid extends StatelessWidget {
//   const ServiceGrid({super.key});

//   @override
//   Widget build(BuildContext context) {
//     final serviceProvider = Provider.of<HomeProvider>(context);
//     final services = serviceProvider.services;

//     return SizedBox(
//       height: 250,
//       child: GridView.builder(
//         gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
//           crossAxisCount: 6,
//           crossAxisSpacing: 2.0,
//           mainAxisSpacing: 15.0,
//         ),
//         itemCount: services.length,
//         itemBuilder: (context, index) {
//           final service = services[index];

//           return ServiceCard(service: service);
//         },
//       ),
//     );
//   }
// }

// class ServiceCard extends StatelessWidget {
//   final Service service;

//   const ServiceCard({
//     super.key,
//     required this.service,
//   });

//   @override
//   Widget build(BuildContext context) {
//     return Consumer<AuthProvider>(builder: (authContext, authProvider, _) {
//       return InkWell(
//         onTap: () {
//           final String serviceCode = service.serviceCode;
//           final int totalAmount = service.serviceTariff;

//           Navigator.of(context).push(
//             MaterialPageRoute(
//               builder: (context) => Transaksi2(
//                 serviceCode: serviceCode,
//                 totalAmount: totalAmount,
//               ),
//             ),
//           );
//         },
//         child: Column(
//           children: <Widget>[
//             Image.network(
//               service.serviceIcon,
//               width: 50,
//               height: 50,
//               fit: BoxFit.contain,
//             ),
//             Expanded(
//               child: Center(
//                 child: Text(
//                   service.serviceName,
//                   style: const TextStyle(
//                     fontWeight: FontWeight.bold,
//                     fontSize: 12,
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       );
//     });
//   }
// }
