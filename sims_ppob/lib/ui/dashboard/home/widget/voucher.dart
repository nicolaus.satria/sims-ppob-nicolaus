// import 'package:carousel_slider/carousel_slider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/model/banner.dart';
import 'package:sims_ppob/ui/dashboard/home/provider/home_provider.dart';
import 'package:sims_ppob/ui/login/provider/login_provider.dart';

class Voucher extends StatelessWidget {
  const Voucher({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AuthProvider>(builder: (authContext, authProvider, _) {
      final bannerProvider = Provider.of<HomeProvider>(context);
      final token = authProvider.token;

      return FutureBuilder<BannerResponse>(
        future: bannerProvider.fetchBanners(token),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Text('Error: ${snapshot.error}');
          } else if (!snapshot.hasData || snapshot.data!.data.isEmpty) {
            return const Text('Tidak ada data banner.');
          } else {
            final banners = snapshot.data!.data;

            return SizedBox(
              width: MediaQuery.of(context).size.width,
              child: CarouselSlider.builder(
                itemCount: banners.length,
                itemBuilder: (_, index, __) => carditem(banners, index),
                options: CarouselOptions(
                  height: MediaQuery.of(context).size.height * 0.15,
                  viewportFraction: 0.8,
                  initialPage: 0,
                  autoPlay: true,
                ),
              ),
            );
          }
        },
      );
    });
  }

  Widget carditem(List<BannerItem> banners, int index) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 4),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: Image.network(
          banners[index].bannerImage, // Menggunakan URL gambar dari data banner
          width: double.infinity,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
