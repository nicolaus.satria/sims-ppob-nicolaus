import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/ui/dashboard/home/provider/home_provider.dart';
import 'package:sims_ppob/ui/dashboard/home/widget/menu.dart';
import 'package:sims_ppob/ui/dashboard/home/widget/profile_name.dart';
import 'package:sims_ppob/ui/dashboard/home/widget/saldo.dart';
import 'package:sims_ppob/ui/dashboard/home/widget/voucher.dart';
import 'package:sims_ppob/ui/login/provider/login_provider.dart';

class Home extends StatelessWidget {
  Home({super.key});

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context, listen: false);
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    final token = authProvider.token;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leadingWidth: 40,
        leading: Padding(
          padding: const EdgeInsets.only(left: 16.0),
          child: Image.asset(
            'assets/images/Logo.png',
          ),
        ),
        titleSpacing: 5,
        title: const Text(
          "SIMS PPOB",
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          FutureBuilder<void>(
            future: homeProvider.fetchProfile(token),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return const Icon(Icons.error, color: Colors.red);
              } else {
                final profile = homeProvider.profiles.isNotEmpty
                    ? homeProvider.profiles.first
                    : null;
                if (profile != null) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage(profile.profileImage),
                    ),
                  );
                } else {
                  return const Icon(Icons.person);
                }
              }
            },
          ),
        ],
      ),
      body: Container(
        margin: const EdgeInsets.all(16),
        color: Colors.white,
        child: const Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ProfileName(),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 24.0),
              child: Saldo(),
            ),
            ServiceMenuGrid(),
            Spacer(),
            Text(
              "Temukan Promo Menarik",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            Voucher(),
          ],
        ),
      ),
    );
  }
}
