import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:sims_ppob/ext/my_list.dart';

class Dashboard extends StatelessWidget {
  final RxInt _indexNumber = 0.obs;

  Dashboard({super.key});

  void _onBottomNavigationTapped(int index) {
    if (index == 0) {}
    _indexNumber.value = index;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Obx(
          () => MyList.fragmentScreen[_indexNumber.value],
        ),
      ),
      bottomNavigationBar: Obx(
        () => BottomNavigationBar(
          backgroundColor: Colors.white,
          currentIndex: _indexNumber.value,
          onTap: _onBottomNavigationTapped,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          iconSize: 28,
          selectedLabelStyle:
              const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          unselectedLabelStyle:
              const TextStyle(fontWeight: FontWeight.bold, fontSize: 14),
          selectedItemColor: Colors.black,
          unselectedItemColor: Colors.grey,
          items: List.generate(
            4,
            (index) {
              var navButtonProperty = MyList.navigationButtonsProperties[index];
              return BottomNavigationBarItem(
                  icon: Icon(navButtonProperty["non_active_icon"]),
                  activeIcon: Icon(navButtonProperty["active_icon"]),
                  label: navButtonProperty["label"]);
            },
          ),
        ),
      ),
    );
  }
}
