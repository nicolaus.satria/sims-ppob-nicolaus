import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:sims_ppob/ui/dashboard/dashboard.dart';
import 'package:sims_ppob/ui/dashboard/home/home.dart';
import 'package:sims_ppob/ui/dashboard/home/provider/home_provider.dart';
import 'package:sims_ppob/ui/login/login.dart';
import 'package:sims_ppob/ui/login/provider/login_provider.dart';
import 'package:sims_ppob/ui/profile/provider/profile_provider.dart';
import 'package:sims_ppob/ui/register/register.dart';
import 'package:sims_ppob/ui/topup/provider/topup_provider.dart';
import 'package:sims_ppob/ui/transaction/provider/transaction_provider.dart';
import 'package:sims_ppob/ui/transaction/widget/transaction_history.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthProvider()),
        ChangeNotifierProvider(create: (_) => HomeProvider()),
        ChangeNotifierProvider(create: (_) => TopUpProvider()),
        ChangeNotifierProvider(create: (_) => TransactionProvider()),
        ChangeNotifierProvider(create: (_) => TransactionHistoryProvider()),
        ChangeNotifierProvider(create: (_) => ProfileProvider()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.white),
          useMaterial3: true,
        ),
        initialRoute: '/login',
        routes: {
          '/login': (context) => const LoginScreen(),
          '/register': (context) => const RegisterView(),
          '/home': (context) => Home(),
          '/dashboard': (context) => Dashboard(),
          '/service': (context) => const TransactionHistoryPage(),
        },
        builder: EasyLoading.init(),
      ),
    ),
  );
}
